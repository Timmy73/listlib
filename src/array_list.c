#include <list/array_list.h>

array_list_t new_array_list(){
    array_list_t list = malloc(sizeof(struct array_list));

    list->size      = 2;
    list->cur_index = 0
    list->array     = malloc(sizeof(void *) * list->size);

    return list;
}

int add(array_list_t list, void *obj){
    if(list == NULL)
        return -1;

    if(list->cur_index >= list->size){
        list->size  = list->size << 1;
        list->array = realloc(list->array, sizeof(void *) * list->size);

        if(list->array == NULL)
            return -1;
    }

    list->array[list->cur_index] = obj;
    list->cur_index++;

    return 0;
}

int add_at(array_list_t list, int i, void *obj){
    if(list == NULL || i < 0)
        return -1;

    while(list->size >= i && list->array != NULL){
        list->size  = list->size << 1;
        list->array = realloc(list->array, sizeof(void *) * list->size);
    }

    if(list->array == NULL)
        return -1;

    list->array[i]  = obj;
    list->cur_index = i;

    return 0;
}

int add_all(array_list_t list, array_list_t elems){

}

int add_array(array_list_t list, void **elems){

}

int contains(array_list_t list,void *obj){

}

int index_of(array_list_t list,void *obj){

}

int is_empty(array_list_t list,void *obj){

}

int size(array_list_t list){

}

void *remove_at(array_list_t list,int i){

}

void remove_obj(array_list_t list,void *obj){

}

void clear(array_list_t list){

}

void update(array_list_t list,int i, void *obj){

}

void destruct(array_list_t list){

}
