export BINDIR = $(CURDIR)/bin
export SRCDIR = $(CURDIR)/src
export LIBDIR = $(CURDIR)/lib
export INCDIR = $(CURDIR)/include

APP_NAME = listlib

CC = gcc
CFLAGS = -Wall -Werror -Wextra -I $(INCDIR)

ifeq ($(DEBUG),1)
CFLAGS += -g
endif

C_SRC = $(wildcard $(SRCDIR)/*.c)
OBJ   = $(C_SRC:.c=.o)

.PHONY: clean

all: dir $(APP_NAME)

dir:
	mkdir -p $(BINDIR)

$(APP_NAME): $(OBJ)
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $^

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf $(BINDIR)
	rm -rf $(SRCDIR)/*.o
