#ifndef __ARRAY_LIST_H__
#define __ARRAY_LIST_H__

#include <stdlib.h>
#include <stdio.h>

typedef struct array_list{

    void **array;
    int size;
    int cur_index;

} *array_list_t;


array_list_t new_array_list();

int add(array_list_t list, void *obj);
int add_at(array_list_t list, int i, void *obj);
int add_all(array_list_t list, array_list_t elems);
int add_array(array_list_t list, void **elems);
int contains(array_list_t list,void *obj);
int index_of(array_list_t list,void *obj);
int is_empty(array_list_t list,void *obj);
int size(array_list_t list);

void *remove_at(array_list_t list,int i);
void remove_obj(array_list_t list,void *obj);

void clear(array_list_t list);
void update(array_list_t list,int i, void *obj);
void destruct(array_list_t list);

#endif //__ARRAY_LIST_H__
